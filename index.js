var gamepad = require("gamepad");
var sb = require("./sbrick")

var axisValue = [],
 vel1 = 0,
 vel2 = 0,
 factor1 = 10.0,
 factor2 = 10.0,
 greenTrain = false,
 redTrain = false;

const states = {
  incrementing: 1,
  proportional: 2
};
var state = states.incrementing;
 
// Initialize the library
gamepad.init()


// Create a game loop and poll for events
setInterval(gamepad.processEvents, 16);
// Scan for new gamepads as a slower rate
setInterval(gamepad.detectDevices, 500);
 
// Listen for move events on all gamepads
gamepad.on("move", function (id, axis, value) {
  if (Math.abs(value) < 0.2) {
    axisValue[axis] = 0.0;
  } else {
    axisValue[axis] = -value;
  }
});

 
// Listen for button down events on all gamepads

gamepad.on("down", function (id, num) {
  switch (num) {
    case 0:
      state = states.incrementing;
      vel2 = 0;
      factor2 = 10.0;
      break;
    case 2:
      state = states.incrementing;
      vel2 = 0;
      factor2 = -10.0;
      break;
    case 3:
      state = states.incrementing;
      vel1 = 0;
      factor1 = 10.0;
      break;
    case 5:
      state = states.incrementing;
      vel1 = 0;
      factor1 = -10.0;
      break;
    case 1:
      // falls through
    case 4:
      state = states.proportional;
      vel1 = 0;
      vel2 = 0;
      break;
    case 6:
      vel1 = 0;
      break;
    case 7:
      vel2 = 0;
      break;
    default:
      break;
  }
});
 
function doIncrement (vel, factor, value) {
  var newVel = vel;

  newVel += factor * value;
  if (factor > 0) {
    newVel = Math.min (255, newVel);
    newVel = Math.max (0, newVel);
  }
  if (factor < 0) {
    newVel = Math.max (-255, newVel);
    newVel = Math.min (0, newVel);
  }
  return newVel;
}

function incrementVel () {
  if (1 in axisValue) {
    vel1 = doIncrement(vel1, factor1, axisValue[1]);
  }
  if (4 in axisValue) {
    vel2 = doIncrement(vel2, factor2, axisValue[4]);
  }
}

function setVelProportional() {
  if (1 in axisValue) {
    vel1 = axisValue[1] * 255;
  }
  if (4 in axisValue) {
    vel2 = axisValue[4] * 255;
  }
  
}

function loop () {
    if (!redTrain) {
        if ('0007802e21c0' in sb.sbricks) {
            redTrain = sb.sbricks['0007802e21c0']
        }
    }
    if (!greenTrain) {
        if ('0007802e39c0' in sb.sbricks) {
            greenTrain = sb.sbricks['0007802e39c0']
        }
    }
    switch (state) {
        case states.incrementing: {
            incrementVel();
            break;
        }
        case states.proportional: {
            setVelProportional();
            break;
        }
        default:
            break;
    }
    if (redTrain) {
        if (redTrain.isConnected()) {
            redTrain.move(3, vel1);
        } else {
            vel1 = 0;
        }
    }
    if (greenTrain) {
        if (greenTrain.isConnected()) {
            greenTrain.move(3, vel2);
        } else {
            vel2 = 0;
        }
    }
    //console.log(vel1 + '  ' + vel2);
}

setInterval(loop, 100);
